Mesap4 und die zugehörigen Oberflächen nähern sich ihrem Lebensende. In Rahmen dieses Projekt setzt das Umweltbundesamt eine Neuimplementierung der Mesap-Benutzerschnittstelle auf Basis des Mesap5-Frameworks und moderner Oberflächentechnologien um.

[Inhalte im Wiki!](https://gitlab.opencode.de/uba-emsit/mesap-admins/Mesap5UBA/-/wikis/home)
